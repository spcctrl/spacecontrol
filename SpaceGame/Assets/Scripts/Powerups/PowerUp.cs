﻿using System.Collections;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public GameSceneManager.POWERUP powerup;
    private soundBank mySoundBank;
    public GameObject shieldPowerPrefab;

    void Start()
    {
        mySoundBank = FindObjectOfType<soundBank>();
    }

    private void ApplyPowerup(PlayerController powered)
    {
        mySoundBank.PlayBonusSound();
        switch (powerup)
        {
            case GameSceneManager.POWERUP.DAMAGE:
                powered.shotStrength = Mathf.Clamp(powered.shotStrength++,1,3);
                break;
            case GameSceneManager.POWERUP.SPEED:
                powered.shotInterval = Mathf.Clamp(powered.shotInterval - 0.2f, 0.2f, 1f);
                break;
            case GameSceneManager.POWERUP.HEALTH:
                powered.gameObject.GetComponent<HandleHits>().hitCapacity = Mathf.Clamp(powered.gameObject.GetComponent<HandleHits>().hitCapacity+1, 0, 3);
                powered.gameObject.GetComponent<HandleHits>().UpdateLives();
                break;
            case GameSceneManager.POWERUP.SHIELD:
                powered.gameObject.GetComponent<HandleHits>().shielded = true;
                GameObject tempObject = Instantiate(shieldPowerPrefab, powered.transform, false);
                tempObject.transform.position = powered.transform.position;
                //add visual shield. also create functionality
                break;
            case GameSceneManager.POWERUP.MULTISHOT:
                powered.isMultiShot = true;
                // add more shots, create functionality
                break;
            default:
                break;
    
        }
    }


    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag.Equals("Player"))
            ApplyPowerup(collider.GetComponent<PlayerController>());
    }

}
