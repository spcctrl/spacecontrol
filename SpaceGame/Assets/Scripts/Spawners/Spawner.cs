﻿using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] obstaclePrefab;
    public GameObject[] bonusPrefab;
    public GameObject[] enemyPrefab;
    public float obstacleSpawnInterval, bonusSpawnInterval, enemySpawnInterval;
   
    public void SpawnObjectRandom(GameObject ObjectPrefab)
    {

        Vector2 spawnPoint = new Vector2(GameSceneManager.instance.screenSizeWorld.x ,Random.Range(-GameSceneManager.instance.screenSizeWorld.y, GameSceneManager.instance.screenSizeWorld.y));
        Instantiate(ObjectPrefab, spawnPoint, ObjectPrefab.transform.rotation);
    }

    private IEnumerator ObstacleSpawnCycle()
    {
        while(true)
        {
            SpawnObjectRandom(obstaclePrefab[Random.Range(0,obstaclePrefab.Length-1)]);
            yield return new WaitForSeconds(obstacleSpawnInterval);
        }
    }

    private IEnumerator BonusSpawnCycle()
    {
        while (true)
        {
            SpawnObjectRandom(bonusPrefab[Random.Range(0, bonusPrefab.Length - 1)]);
            yield return new WaitForSeconds(bonusSpawnInterval);
        }
    }

    private IEnumerator EnemySpawnCycle()
    {
        while (true)
        {
            SpawnObjectRandom(enemyPrefab[Random.Range(0, enemyPrefab.Length - 1)]);
            yield return new WaitForSeconds(enemySpawnInterval);
        }
    }

    private void Start()
    {
        StartCoroutine(ObstacleSpawnCycle());
        StartCoroutine(BonusSpawnCycle());
        StartCoroutine(EnemySpawnCycle());

    }
}
