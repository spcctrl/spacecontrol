﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour {

    public int shotStrength;
    public bool isShotByPlayer;
    public float velocity;

	// Use this for initialization
	
	// Update is called once per frame
	void Update () {
        //        transform.position = new Vector3(transform.position.x  + (isShotByPlayer? 1: -1) * velocity, transform.position.y);
        transform.Translate(transform.right * velocity * (isShotByPlayer? 1 : -1));
    }
}
