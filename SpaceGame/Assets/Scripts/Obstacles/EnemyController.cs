﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyController : MonoBehaviour
{
    public Vector2 velocity2;
    private float maxY;
    private float minY;
    private float RandomDir;
    int dir = 1;

    public float ShotInterval;
    public GameObject ShotPrefab;



    private void Start()
    {

        maxY = Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f)).y;
        minY = -Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f)).y;
        RandomDir = UnityEngine.Random.Range(-1f, 1f);
        StartCoroutine(Shoot());


    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {

        if (transform.position.y >= maxY || transform.position.y <= minY)
        {
            dir *= -1;
        }

        transform.Translate((-Vector3.right * velocity2.x + transform.up * dir * velocity2.y * RandomDir));
    }

    private IEnumerator Shoot()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(ShotInterval);
            Instantiate(ShotPrefab, transform.position, Quaternion.identity);

        }
    }

}