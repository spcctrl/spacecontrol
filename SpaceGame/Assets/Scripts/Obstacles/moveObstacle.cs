﻿using UnityEngine;

public class moveObstacle : MonoBehaviour {

    private Vector2 velocity;

    public float angleRange;
    public bool isSpinning;
    public float MaxSpinFactor;
    private float spinFactor;

    private void Start()
    {
        spinFactor = Random.Range(-MaxSpinFactor, MaxSpinFactor);
        velocity = (-transform.right * 0.2f) + (transform.up * Random.Range(-angleRange, angleRange));

    }
    // Update is called once per frame
    
	// Update is called once per frame
	void Update () {
        transform.Translate(velocity);
        if (isSpinning)
        {
            transform.Rotate(Vector3.right * Time.deltaTime * spinFactor);
         //   transform.Rota te(Vector3.up * Time.deltaTime * spinFactor);
        }
    }
}
