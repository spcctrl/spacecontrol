﻿using UnityEngine;
using UnityEngine.UI;
public class HandleHits : MonoBehaviour
{

	public int hitCapacity;
	public bool shielded = false;
	private soundBank mySoundBank;
	public Image[] lives; 

	public GameObject Splosion;

	// Use this for initialization
	void Start () {
		mySoundBank = FindObjectOfType<soundBank>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter2D(Collider2D collider)
	{
		if (!collider.tag.Equals("pickup"))
		{
			GetHit(collider);
		}

	}

	private void GetHit(Collider2D collider)
	{
		if (collider.GetComponent<Shot>() != null && !shielded)
		{
			hitCapacity -= collider.GetComponent<Shot>().shotStrength;
			mySoundBank.PlayRandomHitSound();
            if (collider.tag.Equals("Player"))
            {
                UpdateLives();
            }    
        }
		else
		{
            if (!shielded)
            {
                hitCapacity--;
                mySoundBank.PlayRandomHitSound();
            }

            if (gameObject.tag.Equals("Player"))
            {
                UpdateLives();
            }
        }


		if (hitCapacity <= 0)
		{
			DieHard();
		}

	}

	public void UpdateLives()
	{
		switch (hitCapacity)
		{
			case 0:
				lives[0].gameObject.SetActive(false);
				lives[1].gameObject.SetActive(false);
				lives[2].gameObject.SetActive(false);
				break;

			case 1:
				lives[0].gameObject.SetActive(true);
				lives[1].gameObject.SetActive(false);
				lives[2].gameObject.SetActive(false);
				break;

			case 2:
				lives[0].gameObject.SetActive(true);
				lives[1].gameObject.SetActive(true);
				lives[2].gameObject.SetActive(false);
				break;
			case 3:
				lives[0].gameObject.SetActive(true);
				lives[1].gameObject.SetActive(true);
				lives[2].gameObject.SetActive(true);
				break;
		}
	}

	private void DieHard()
	{
		if(Splosion)
		{ 
			GameObject psTemp = Instantiate(Splosion, transform.position, Quaternion.identity);

			psTemp.GetComponent<ParticleSystem>().startColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
			psTemp.GetComponent<ParticleSystem>().startSize = Random.Range(0.1f, 1f);
		}
		
		// Do explosion stuff
		if (gameObject.tag == "Player")
		{
			Debug.Log("Dies");

			if (GameSceneManager.instance.timer > MenuManager.CurrentHighScore)
			{
				MenuManager.CurrentHighScore = GameSceneManager.instance.timer;
			}
			GameSceneManager.instance.StopTimer();


			mySoundBank.PlayDieSound();

			GameSceneManager.instance.doneText.color = Color.cyan;
			GameSceneManager.instance.timeText.color = Color.green;
			MenuManager.instance.LoadSceneWithDelay(0, 0.8f);
			
		}

		Destroy(gameObject);
	}

}
