﻿using UnityEngine;

public class EventManager : MonoBehaviour {

    public delegate void PlayerEvent(int eventParam);

    public static event PlayerEvent playerHit;
    public static event PlayerEvent playerPickUp;

}
