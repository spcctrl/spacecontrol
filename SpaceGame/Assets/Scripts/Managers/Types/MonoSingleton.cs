﻿using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T:MonoBehaviour {
        
    /*
     * lazy singleton
     */
    T _instance;

    private static object _lock = new object();

    public static T instance;

    // Use this for initialization
    public void Start () {

        _instance = gameObject.GetComponent<T>();

        if (instance == null)
        {
            instance = _instance;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(instance);
	}
}