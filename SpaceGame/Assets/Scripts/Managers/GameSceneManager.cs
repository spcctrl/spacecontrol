﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneManager : MonoSingleton<GameSceneManager> {

	public float timer = 0;
	public Text timeText;
	public Text doneText;
	private soundBank mySoundBank;
	private bool isHyperSpeed = false;

	public Vector3 screenSizeWorld;
	public Vector3 screenZeroWorld;
	private bool timerUpdates = true;

	private void Awake()
	{
		screenSizeWorld = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
		screenZeroWorld = Camera.main.ScreenToWorldPoint(Vector2.zero);
	}

	private void Start()
	{
		base.Start();

		mySoundBank = FindObjectOfType<soundBank>();
	}

	public enum POWERUP
	{
		DAMAGE,
		SPEED,
		HEALTH,
		SHIELD,
		MULTISHOT
	}

	public static float scrollSpeed;
	public float amplitudeModifier;
	
	// Update is called once per frame
	private void GenerateSpeed(float amplitudeModifier) {
		
		scrollSpeed = Mathf.Clamp(Mathf.PingPong(Time.timeSinceLevelLoad/8, 2f), 0.2f, 1f);
		if (scrollSpeed >= 0.7f && isHyperSpeed == false)
		{
			isHyperSpeed = true;
			mySoundBank.PlayhypespeedOnSound();
		}
		if (scrollSpeed < 0.7f && isHyperSpeed == true)
		{
			isHyperSpeed = false;
			mySoundBank.PlayhypespeedOffSound();
		}

	}

	public void FixedUpdate()
	{
			   
		GenerateSpeed(amplitudeModifier);

		if (timerUpdates)
		{
			Time.timeScale = scrollSpeed;

			// show time on timer

			timer += Time.deltaTime;
			timeText.text = timer.ToString();
		}
	}

	public void StopTimer()
	{
		timerUpdates = false;
	}

}
