﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoSingleton<MenuManager>
{
    public Text currentHighScoreText;
    public static float CurrentHighScore;


    public void LoadScene(int sceneNum)
    {
        Application.LoadLevelAsync(sceneNum);
    }

    public void LoadSceneWithDelay(int sceneNum, float seconds)
    {
        StartCoroutine(WaitAndLoadScene(sceneNum, seconds));
        
    }

    private IEnumerator WaitAndLoadScene(int sceneNum, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Application.UnloadLevel(Application.loadedLevel);

        Application.LoadLevelAsync(sceneNum);
    }

    private void OnLevelWasLoaded(int level)
    {
        if (level.Equals(0))
        {
            currentHighScoreText = FindObjectOfType<Text>();
            GameSceneManager.scrollSpeed = 0f;
            if (CurrentHighScore > 0f)
            {
                currentHighScoreText.text = "Current High Score: " + CurrentHighScore.ToString();
            }
            else currentHighScoreText.text = "";
            GameObject.Find("Play Button").GetComponent<Button>().onClick.AddListener(() => LoadScene(1));
        }
    }
}
