﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour {

    public void ClampPositionToScreen()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, GameSceneManager.instance.screenZeroWorld.x, GameSceneManager.instance.screenSizeWorld.x), Mathf.Clamp(transform.position.y, GameSceneManager.instance.screenZeroWorld.y, GameSceneManager.instance.screenSizeWorld.y), transform.position.z);
    }
}
