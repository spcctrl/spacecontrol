﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundBank : MonoBehaviour {

    public AudioClip[] hitSounds;
    public AudioClip bonusSound;
    public AudioClip dieSound;
    public AudioClip hyperspeedOnSound;
    public AudioClip hyperspeedOffSound;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayRandomHitSound()
    {
        GetComponent<AudioSource>().PlayOneShot(hitSounds[Random.Range(0, hitSounds.Length)]);
    }

    public void PlayBonusSound()
    {
        GetComponent<AudioSource>().PlayOneShot(bonusSound);
    }

    public void PlayDieSound()
    {
        GetComponent<AudioSource>().PlayOneShot(dieSound);
    }

    public void PlayhypespeedOnSound()
    {
        GetComponent<AudioSource>().PlayOneShot(hyperspeedOnSound);
    }

    public void PlayhypespeedOffSound()
    {
        GetComponent<AudioSource>().PlayOneShot(hyperspeedOffSound);
    }


}
