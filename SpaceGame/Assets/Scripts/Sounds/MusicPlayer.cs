﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MusicPlayer : MonoBehaviour
{

	private static MusicPlayer instance = null;
	public static MusicPlayer Instance
	{
		get { return instance; }
	}

	public AudioSource IntroMusicSource;
	public AudioSource Loop1Source;
	public AudioSource Loop2Source;
	public AudioSource Loop3Source;
	public float intensity = 0;
	private Slider SpeedSlider;

	void Awake()
	{
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	    
	}

	// Use this for initialization
	void Start () {
		IntroMusicSource.Play();
		Loop1Source.Play();
		Loop2Source.Play();
		Loop3Source.Play();

	}
	
	// Update is called once per frame
	void Update () {
		intensity = GameSceneManager.scrollSpeed;
	    if (intensity > 0.1 && !SpeedSlider)
	    {
	        SpeedSlider = FindObjectOfType<Slider>();
	    }
		if (SpeedSlider)
		{
			SpeedSlider.value = intensity;
		}
		Debug.Log("intensity is now " + intensity);
		IntroMusicSource.volume = AdjustLoop0();
		Loop1Source.volume = AdjustLoop1 ();
		Loop2Source.volume = AdjustLoop2 ();
		Loop3Source.volume = AdjustLoop3 ();
	}
		
	float AdjustLoop0()
	{
		if (intensity < 0.1f)
		{
			return 1;
		} else
		{
			return 0;
		}
	}


	float AdjustLoop1()
	{
		if (intensity < 0.1f)
		{
			return 0;
		}
		else if (intensity <= 0.4f)
		{
			return 1;
		}
		else if (intensity >= 0.4f || intensity <= 0.5f)
		{
			return (1f - (intensity - 0.4f) * 10);
		}
		else if (intensity > 0.5f)
		{
			return 0;
		}

		return 0;
	}

	float AdjustLoop2()
	{
		if (intensity < 0.4f)
		{
			return 0;
		} else if (intensity >= 0.4f || intensity <= 0.5f)
		{
			return ((intensity - 0.4f) * 10);
		} else if (intensity > 0.5f || intensity < 0.6f)
		{
			return 1;
		} else if (intensity >= 0.7f || intensity <= 0.8f)
		{
			return (1f - (intensity - 0.7f) * 10);
		} else if (intensity > 0.8f)
		{
			return 0;
		}

		return 0f;
	}

	float AdjustLoop3()
	{
		if (intensity < 0.7f)
		{
			return 0;
		} else if (intensity >= 0.7f || intensity <= 0.8f)
		{
			return ((intensity - 0.7f) * 10);
		} else if (intensity > 0.8f)
		{
			return 1;
		}

		return 0f;
	}
}
