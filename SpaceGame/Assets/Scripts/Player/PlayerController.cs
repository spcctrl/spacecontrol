﻿using System.Collections;
using UnityEngine;

public class PlayerController : ScreenController {

    public float shotInterval;
    public int shotStrength;
    public GameObject MultiShotPrefab;
    public bool isMultiShot = false;

    public Transform shotSpawn;

    public GameObject[] ShotPrefab;

	// Use this for initialization
	void Start () {

        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(transform.localScale.x + 25f , (Screen.height/2f) + transform.localScale.y, 0f));
       
        StartCoroutine(Shoot());
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        Move();
        ClampPositionToScreen();

    }

    private void Move()
    {
        if (Input.touchCount != 0)
        {
            transform.position = transform.position + ((Vector3)Input.GetTouch(0).deltaPosition / 70f);
        }
    }

    private IEnumerator Shoot()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(shotInterval);

            Instantiate(isMultiShot? MultiShotPrefab : ShotPrefab[shotStrength], shotSpawn.position, Quaternion.identity);
            
        }
    }

}
