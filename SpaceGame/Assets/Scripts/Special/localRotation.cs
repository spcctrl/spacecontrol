﻿using UnityEngine;

public class localRotation : MonoBehaviour {

    public float rotationSpeed;

	// Update is called once per frame
	void Update ()
    { 
        transform.Rotate(new Vector3(0, rotationSpeed, 0));		
	}
}
