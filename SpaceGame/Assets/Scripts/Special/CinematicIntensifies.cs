﻿using UnityEngine;

public class CinematicIntensifies : MonoBehaviour {

	public Transform player;

	public float minDistance, maxDistance;

	private Vector3 originalPos;


	private void Start()
	{
		originalPos = transform.position;
	}

	// Update is called once per frame
	private void Update () {

		float distanceFromPlayer = Mathf.Lerp(minDistance, maxDistance, Time.timeScale/3.5f);
		if (player)
		{
			transform.position = originalPos + new Vector3(0f, player.position.y - distanceFromPlayer, 0f);
		}

	}
}
