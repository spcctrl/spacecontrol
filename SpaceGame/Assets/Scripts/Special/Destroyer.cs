﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

    public float lifeTime;
     
	// Use this for initialization
	void Start () {
        StartCoroutine(DestroyMe());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator DestroyMe()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(lifeTime);
            if (gameObject.transform.parent)
            {
                if (gameObject.transform.parent.tag == "Player")
                {
                    gameObject.transform.parent.GetComponent<HandleHits>().shielded = false;
                }
            }


            Destroy(gameObject);
        }
    }
}
